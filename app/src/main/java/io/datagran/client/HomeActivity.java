package io.datagran.client;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    EditText workspaceIDEditText, appKeyEditText;
    Button saveButton;
    CheckBox customCheckbox;
    CheckBox defaultCheckbox;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ImageView closeImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_home);

        workspaceIDEditText = findViewById(R.id.workspaceID);
        appKeyEditText = findViewById(R.id.datasourceID);
        saveButton = findViewById(R.id.saveConf);
        defaultCheckbox = findViewById(R.id.defaultCheckBox);
        customCheckbox = findViewById(R.id.customCheckBox);
        closeImageView = findViewById(R.id.closeImageView);

        prefs = getSharedPreferences("Datagran", MODE_PRIVATE);
        editor = getSharedPreferences("Datagran", MODE_PRIVATE).edit();

        if(prefs.getBoolean("conf", true)) {
            editor.putBoolean("conf", true);
            editor.apply();
            workspaceIDEditText.setText(R.string.datagran_workspace_id);
            appKeyEditText.setText(R.string.datagran_app_key);
            workspaceIDEditText.setEnabled(false);
            appKeyEditText.setEnabled(false);
            defaultCheckbox.setChecked(true);
        } else {
            workspaceIDEditText.setText(prefs.getString("custom_workspace_id", ""));
            appKeyEditText.setText(prefs.getString("custom_app_key", ""));
            workspaceIDEditText.setEnabled(true);
            appKeyEditText.setEnabled(true);
            customCheckbox.setChecked(true);
        }


        defaultCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    customCheckbox.setChecked(false);
                    workspaceIDEditText.setText(R.string.datagran_workspace_id);
                    appKeyEditText.setText(R.string.datagran_app_key);
                    workspaceIDEditText.setEnabled(false);
                    appKeyEditText.setEnabled(false);
                }
            }
        });

        customCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    defaultCheckbox.setChecked(false);
                    workspaceIDEditText.setText(prefs.getString("custom_workspace_id", ""));
                    appKeyEditText.setText(prefs.getString("custom_app_key", ""));
                    workspaceIDEditText.setEnabled(true);
                    appKeyEditText.setEnabled(true);
                }
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });

        closeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(HomeActivity.this, LoginActivity.class);
                if (prefs.getBoolean("launch", true)) {
                    login.putExtra("update", "yes");
                    editor.putBoolean("launch", false);
                    editor.putBoolean("conf", true);
                    editor.apply();
                } else {
                    login.putExtra("update", "no");
                }

                startActivity(login);
                finish();
            }
        });
    }

    private void validateFields() {
        String inputWorkspaceID = workspaceIDEditText.getText().toString();
        String inputAppKey = appKeyEditText.getText().toString();
        if(inputWorkspaceID.equalsIgnoreCase("") || inputAppKey.equalsIgnoreCase("")){
            Toast.makeText(HomeActivity.this, "Field must not be empty", Toast.LENGTH_SHORT).show();
        } else if (inputWorkspaceID.length() < 22 || inputAppKey.length() < 22) {
            Toast.makeText(HomeActivity.this, "The fields should have atleast 22 characters", Toast.LENGTH_SHORT).show();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(prefs.getBoolean("launch", true)) {
                        editor.putBoolean("launch", false);
                    }
                    if (defaultCheckbox.isChecked()) {
                        editor.putBoolean("conf", true);
                        editor.apply();

                        Intent login = new Intent(HomeActivity.this, LoginActivity.class);
                        login.putExtra("update", "yes");
                        startActivity(login);
                        finish();
                    } else {
                        AlertDialog alertDialog = new AlertDialog.Builder(HomeActivity.this).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setCancelable(false);
                        alertDialog.setMessage("Please double check the values as the app can't validate it!");
                        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                editor.putBoolean("conf", false);
                                editor.putString("custom_workspace_id", inputWorkspaceID);
                                editor.putString("custom_app_key", inputAppKey);
                                editor.apply();

                                Intent login = new Intent(HomeActivity.this, LoginActivity.class);
                                login.putExtra("update", "yes");
                                startActivity(login);
                                finish();
                            }
                        });
                        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alertDialog.show();
                    }
                }
            }, 100);
        }
    }
}
