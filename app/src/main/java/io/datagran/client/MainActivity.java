package io.datagran.client;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.gson.JsonObject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import io.datagran.sdk.android.Tracker;

public class MainActivity extends AppCompatActivity {

    String TAG = MainActivity.class.getName();

    Button buttonOnClick, buttonOnLongClick;
    EditText editTextOnFocusChange, editTextOnKeyPress;
    SeekBar seekBar;
    Switch onOffSwitch;
    Switch geoParam;
    String[] users = { " -- Please select -- ", "Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6" };
    Spinner spin;
    boolean addGeo = false;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //To detect the key events(onKeyPress) from the hardware for ex: volume up and downs.
        Tracker.singleton().track(event, Tracker.EVENT_ID_ON_KEY, MainActivity.this, addGeo);
        return super.onKeyDown(keyCode, event);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences("Datagran", MODE_PRIVATE);
        editor = getSharedPreferences("Datagran", MODE_PRIVATE).edit();

        Toolbar toolbar = findViewById(R.id.toolbar);
        buttonOnClick = findViewById(R.id.button_onClick);
        buttonOnLongClick = findViewById(R.id.button_onLongClick);
        editTextOnFocusChange = findViewById(R.id.editText_onFocusChange);
        editTextOnKeyPress = findViewById(R.id.editText_onKeyPress);
        seekBar = findViewById(R.id.custom_event_1);
        onOffSwitch = findViewById(R.id.custom_event_2);
        geoParam = findViewById(R.id.gps_switch);
        spin = findViewById(R.id.custom_event_3);
        setSupportActionBar(toolbar);

        /*findViewById(R.id.custom_event_name_2).setVisibility(View.GONE);
        findViewById(R.id.custom_event_name_3).setVisibility(View.GONE);
        onOffSwitch.setVisibility(View.GONE);
        spin.setVisibility(View.GONE);*/

        editTextOnFocusChange.setImeOptions(EditorInfo.IME_ACTION_DONE);

        editTextOnFocusChange.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    editTextOnKeyPress.requestFocus();

                    //To detect focus(onfocusChange) change event while press enter from soft keyboard
                    Tracker.singleton().track(editTextOnFocusChange, Tracker.EVENT_ID_FOCUS_CHANGED, MainActivity.this, addGeo);
                    return true;
                }
                return false;

            }
        });


        editTextOnKeyPress.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //To detect the key events(onKeyPress) from the soft keyboard
                Tracker.singleton().track(event, Tracker.EVENT_ID_ON_KEY, MainActivity.this, addGeo);
                return false;
            }
        });

        buttonOnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To detect the key onClickEvent from a view
                Tracker.singleton().track(v, Tracker.EVENT_ID_CLICK,
                        MainActivity.this, addGeo);
            }
        });

        buttonOnLongClick.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //To detect the key onLongClick from a view
                Tracker.singleton().track(v, Tracker.EVENT_ID_LONG_CLICK,
                        MainActivity.this, addGeo);
                return false;
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                JsonObject object = new JsonObject();
                object.addProperty("type", "Distance");
                object.addProperty("range", seekBar.getProgress());
                Tracker.singleton().trackCustom("seekBarProgress", object, MainActivity.this, addGeo);
            }
        });

        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                JsonObject object = new JsonObject();
                object.addProperty("name", buttonView.getTag().toString());
                if(isChecked)
                    object.addProperty("value", "ON");
                else
                    object.addProperty("value", "OFF");

                //editor.putBoolean("onOffSwitch", isChecked);
                //editor.apply();
                Tracker.singleton().trackCustom("customSwitch", object, MainActivity.this, addGeo);
            }

        });

        geoParam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    addGeo = true;
                    Toast.makeText(MainActivity.this, "All further events will carry Geo Info.", Toast.LENGTH_SHORT).show();
                } else {
                    addGeo = false;
                    Toast.makeText(MainActivity.this, "No further events will carry Geo Info.", Toast.LENGTH_SHORT).show();
                }

                editor.putBoolean("addGeo", addGeo);
                editor.apply();
            }

        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, users);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != 0) {
                    JsonObject object = new JsonObject();
                    object.addProperty("name", "Spinner");
                    object.addProperty("value", users[position]);
                    Tracker.singleton().trackCustom("customSpinner", object, MainActivity.this, addGeo);
                } else {
                    //Toast.makeText(MainActivity.this, "Position 0", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        View view = getWindow().getDecorView().findViewById(android.R.id.content);
        Tracker.singleton().trackViews("Main View", view, view, this, addGeo);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!prefs.getBoolean("addGeo", false)) {
            geoParam.setChecked(false);
            addGeo = false;
        } else {
            geoParam.setChecked(true);
            addGeo = true;
        }
        //onOffSwitch.setChecked(prefs.getBoolean("onOffSwitch", false));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //Toast.makeText(MainActivity.this, "Menu option event is initiated", Toast.LENGTH_SHORT).show();
            //To detect the key onOptionItemSelected from a view
            Tracker.singleton().track(item, Tracker.EVENT_ID_OPTIONS_MENU, MainActivity.this, addGeo);
            return true;
        } else if (id == R.id.action_logout) {
            //To detect the key onOptionItemSelected from a view
            Tracker.singleton().track(item, Tracker.EVENT_ID_OPTIONS_MENU, MainActivity.this, addGeo);
            Tracker.singleton().resetDGuserid();
            editor.putBoolean("login", false);
            editor.apply();
            Intent login = new Intent(MainActivity.this, LoginActivity.class);
            login.putExtra("update", "no");
            startActivity(login);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
