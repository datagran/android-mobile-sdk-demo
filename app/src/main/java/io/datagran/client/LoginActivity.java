package io.datagran.client;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.gson.Gson;

import java.util.ArrayList;

import io.datagran.sdk.android.Tracker;

/**
 *  Login Activity for processing the validation from the user input.
 * */

public class LoginActivity extends AppCompatActivity {

    EditText usernameEditText, passwordEditText;
    Button loginButton;
    TextView version;
    ImageView changeConfig;
    ProgressBar loadingProgressBar;
    String TAG_UTM_SOURCE = "utm_source";
    String TAG_UTM_MEDIUM = "utm_medium";
    String TAG_UTM_CAMPAIGN = "utm_campaign";
    String TAG_SOURCE = "source";
    String TAG_MEDIUM = "medium";
    String TAG_CAMPAIGN = "campaign";
    private static final String TAG = "LoginActivity";

    //    String email = "check@mailinator.com";
    //String password = "client@123";

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    private WebView webView = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        setContentView(R.layout.activity_login);
        prefs = getSharedPreferences("Datagran", MODE_PRIVATE);
        editor = getSharedPreferences("Datagran", MODE_PRIVATE).edit();

        if(prefs.getBoolean("launch", true)) {
            Intent home = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(home);
            finish();
            return;
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getString("update") != null) {
            if(extras.getString("update").toString().equals("yes")) {
                if (prefs.getBoolean("conf", true))
                    Tracker.init(this, true, getString(R.string.datagran_app_key), getString(R.string.datagran_workspace_id), 10, 60);
                else
                    Tracker.init(this, true,
                            prefs.getString("custom_app_key", getString(R.string.datagran_app_key)),
                            prefs.getString("custom_workspace_id", getString(R.string.datagran_workspace_id)),10, 60);
            }
        } else {
            if (prefs.getBoolean("conf", true))
                Tracker.init(this, true, getString(R.string.datagran_app_key), getString(R.string.datagran_workspace_id), 10, 60);
            else
                Tracker.init(this, true,
                        prefs.getString("custom_app_key", getString(R.string.datagran_app_key)),
                        prefs.getString("custom_workspace_id", getString(R.string.datagran_workspace_id)), 10, 60);
        }

        //To set the activity on the SDK and requesting for the location permission
        Tracker.singleton().setActivityContext(this);

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        if (pendingDynamicLinkData != null) {
                            Log.d(TAG, "onSuccess: " + new Gson().toJson(pendingDynamicLinkData));
                            ArrayList<String> referrerData = new ArrayList<String>();
                            referrerData.add(TAG_SOURCE);
                            referrerData.add(TAG_MEDIUM);
                            referrerData.add(TAG_CAMPAIGN);
                            referrerData.add(TAG_UTM_SOURCE);
                            referrerData.add(TAG_UTM_CAMPAIGN);
                            referrerData.add(TAG_UTM_MEDIUM);
                            Tracker.singleton().trackReferrer(pendingDynamicLinkData, referrerData, LoginActivity.this, true);
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "getDynamicLink:onFailure", e);
                    }
                });

        usernameEditText = findViewById(R.id.username);
        //usernameEditText.setText("karthikeyan.kandasamy@datagran.io");
        passwordEditText = findViewById(R.id.password);
        //passwordEditText.setText("client@123");
        loginButton = findViewById(R.id.login);
        loadingProgressBar = findViewById(R.id.loading);
        changeConfig = findViewById(R.id.changeConfigLink);
        //changeConfig.setVisibility(View.GONE);
        //checkInstallReferrer();
        webView = findViewById(R.id.webView);

        try {
            String versionName = getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
            version = findViewById(R.id.version);
            version.setText("Version Number: "+versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        alertDialog.setTitle("CONFIRM NAVIGATION");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Please tap on the respective button according to your test case. \n\n" +
                "WebView - To test the Hybrid functionality ( Web + Native View )\n\n" +
                "Native - To test the Native functionality alone\n");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Native View", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                goToNative();
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "WebView", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                goToWebView();
            }
        });
        alertDialog.show();

    }

    public void goToWebView() {
        webView.setVisibility(View.VISIBLE);
        usernameEditText.setVisibility(View.GONE);
        passwordEditText.setVisibility(View.GONE);
        loginButton.setVisibility(View.GONE);
        loadingProgressBar.setVisibility(View.GONE);
        changeConfig.setVisibility(View.GONE);

        DatagranWebInterface jsInterface = new DatagranWebInterface(getApplicationContext(), this);
        webView.addJavascriptInterface(jsInterface, "DatagranWebInterface");

        WebSettings mWebSettings = webView.getSettings();
        mWebSettings.setJavaScriptEnabled(true);
        mWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebSettings.setSupportZoom(false);
        mWebSettings.setAllowFileAccess(true);
        mWebSettings.setAllowContentAccess(true);

        mWebSettings.setBlockNetworkImage(false);
        mWebSettings.setDomStorageEnabled(true);

        webView.loadUrl("https://jitpack-datagran.github.io");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    public void goToNative() {

        if(prefs.getBoolean("login", false)) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
            return;
        }

        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validateUser();
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUser();
            }
        });

        changeConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                finish();
            }
        });

        View view = getWindow().getDecorView().findViewById(android.R.id.content);
        Tracker.singleton().trackViews("Login View", view, view, this, true);

        //task.execute();
    }

    /*AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
        @Override
        protected String doInBackground(Void... params) {
            String advId = null;

            try {
                advId = AdvertisingIdClient.getAdvertisingIdInfo(LoginActivity.this).getId();
            } catch (Exception exception) {
                // Network or server error, try later
                Log.e(TAG, exception.toString());
            }

            return advId;
        }

        @Override
        protected void onPostExecute(String advId) {
            Log.i(TAG, "advId retrieved:" + advId);
        }

    };*/

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     *  Function for validating the user is valid user or invalid.
     *  All the input were validating whether the password is incorrect or not,Email id validation and so on.
     *
     * */

    private void validateUser() {
        String inputEmail = usernameEditText.getText().toString();
        String inputPassword = passwordEditText.getText().toString();
        if(inputEmail.equalsIgnoreCase("") ||
                inputPassword.equalsIgnoreCase("")){
            Toast.makeText(LoginActivity.this, "Field must not be empty", Toast.LENGTH_SHORT).show();
        } else if (inputPassword.length() < 5) {
            Toast.makeText(LoginActivity.this, "Password should be greater than 5 characters", Toast.LENGTH_SHORT).show();
        } else {
            loadingProgressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    editor.putBoolean("login", true);
                    editor.apply();
                    Tracker.singleton().identify(inputEmail,LoginActivity.this,true);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                }
            }, 100);
        }
    }

    /**
     *  Function to check whether the input e-mail is validate or not
     * */

    private boolean isValidEmail(String target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /*private final String prefKey = "checkedInstallReferrer";
    private final Executor backgroundExecutor = Executors.newSingleThreadExecutor();

    void checkInstallReferrer() {
        if (getPreferences(MODE_PRIVATE).getBoolean(prefKey, false)) {
            return;
        }

        InstallReferrerClient referrerClient = InstallReferrerClient.newBuilder(this).build();
        backgroundExecutor.execute(() -> getInstallReferrerFromClient(referrerClient));
    }

    void getInstallReferrerFromClient(InstallReferrerClient referrerClient) {

        referrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        ReferrerDetails response = null;
                        try {
                            response = referrerClient.getInstallReferrer();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            return;
                        }
                        final String referrerUrl = response.getInstallReferrer();


                        // TODO: If you're using GTM, call trackInstallReferrerforGTM instead.
                        //trackInstallReferrer(referrerUrl);
                        ArrayList<String> referrerData = new ArrayList<String>();
                        referrerData.add(TAG_SOURCE);
                        referrerData.add(TAG_MEDIUM);
                        referrerData.add(TAG_CAMPAIGN);
                        referrerData.add(TAG_UTM_SOURCE);
                        referrerData.add(TAG_UTM_CAMPAIGN);
                        referrerData.add(TAG_UTM_MEDIUM);
                        Tracker.singleton().trackReferrer(referrerUrl, referrerData, LoginActivity.this, true, true);

                        // Only check this once.
                        getPreferences(MODE_PRIVATE).edit().putBoolean(prefKey, true).commit();

                        // End the connection
                        referrerClient.endConnection();

                        break;
                    case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                        // API not available on the current Play Store app.
                        break;
                    case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                        // Connection couldn't be established.
                        break;
                }
            }

            @Override
            public void onInstallReferrerServiceDisconnected() {

            }
        });
    }

    public void getInstallTime() throws PackageManager.NameNotFoundException {
        PackageManager pm = LoginActivity.this.getPackageManager();
        PackageInfo packageInfo = pm.getPackageInfo("io.datagran.client", PackageManager.GET_PERMISSIONS);

        Date installTime = new Date( packageInfo.firstInstallTime );
        Log.d(TAG, "Installed: " + installTime.toString());

        Date updateTime = new Date( packageInfo.lastUpdateTime );
        Log.d(TAG, "Updated: " + updateTime.toString());
    }*/

    // Tracker for Classic GA (call this if you are using Classic GA only)
   /* private void trackInstallReferrer(final String referrerUrl) {
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                CampaignTrackingReceiver receiver = new CampaignTrackingReceiver();
                Intent intent = new Intent("com.android.vending.INSTALL_REFERRER");
                intent.putExtra("referrer", referrerUrl);
                receiver.onReceive(getApplicationContext(), intent);
            }
        });
    }

    // Tracker for GTM + Classic GA (call this if you are using GTM + Classic GA only)
    private void trackInstallReferrerforGTM(final String referrerUrl) {
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                InstallReferrerReceiver receiver = new InstallReferrerReceiver();
                Intent intent = new Intent("com.android.vending.INSTALL_REFERRER");
                intent.putExtra("referrer", referrerUrl);
                receiver.onReceive(getApplicationContext(), intent);
            }
        });
    }*/

}
