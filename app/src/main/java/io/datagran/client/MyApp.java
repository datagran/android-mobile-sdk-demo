package io.datagran.client;

import android.app.Application;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import io.datagran.sdk.android.Tracker;

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("Datagran", "App");
        FirebaseApp.initializeApp(this);

        //Tracker.init(this, true, getString(R.string.sdk_app_key),getString(R.string.sdk_workspace_id), 10, 60);
    }

}
