package io.datagran.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import io.datagran.sdk.android.Tracker;

public class DatagranWebInterface {
    private Context mContext;
    private Activity mActivity;
    private boolean addGeo = false;

    /**
     * Instantiate the interface and set the context
     */
    public DatagranWebInterface(Context c, Activity a) {
        this.mContext = c;
        this.mActivity = a;
    }

    /**
     * Show a toast from the web page
     */
    @JavascriptInterface
    public void identify(String userId) {
        if(!userId.isEmpty()) {
            //Toast.makeText(mContext, userId, Toast.LENGTH_LONG).show();
            Tracker.singleton().identify(userId, mContext, addGeo);
        }
    }

    @JavascriptInterface
    public void reset() {
        //Toast.makeText(mContext, "reset", Toast.LENGTH_LONG).show();
        Tracker.singleton().resetDGuserid();
        mContext.startActivity(new Intent(mContext, LoginActivity.class));
        mActivity.finish();
    }

    @JavascriptInterface
    public void trackCustom(String name, String args) throws JSONException {
        JSONObject json = new JSONObject(args);
        if(json != null && json.length() >= 1) {
            //Toast.makeText(mContext, "trackCustom Event "+args, Toast.LENGTH_LONG).show();
            Gson gson = new Gson();
            JsonElement jsonElement = gson.fromJson(json.toString(), JsonElement.class);
            JsonObject payload = gson.fromJson((gson.toJson(jsonElement)), JsonObject.class);
            Tracker.singleton().trackCustom(name, payload, mContext, addGeo);
        } else {
            Toast.makeText(mContext, "Invalid Json", Toast.LENGTH_LONG).show();
        }

    }

    @JavascriptInterface
    public void updateGeoParam(String geoParam) throws JSONException {
        if(!geoParam.isEmpty()) {
            addGeo = Boolean.parseBoolean(geoParam);
            //Toast.makeText(mContext, geoParam, Toast.LENGTH_LONG).show();
        }
    }

}
